/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jeison
 */
public class Taller {
    
    
  /**
 *Realiza el fizzbuzz de 1 hasta n
 *  
 */
    public void fizzbuzz(int n){
        if(n<1) {
            System.err.println("El numero ingresado no es valido");
            return;
        }
        
        int saltos[]={3,2,1,3,1,2,3};
        String fb[]={"fizz","buzz","fizz", "fizz", "buzz","fizz","fizzbuzz"};
        
        int i=0, index =0, ant=1;
        while(i<n){
            
            i+= saltos[index];
            while(ant<i){
                System.out.println(ant+" ");
                ant++;
            }
            ant=i+1;
            System.out.println(i+" "+fb[index]);
            index++;
            index%=7;
        }
    }
    
    public static void main(String[] args) {
        Taller t= new Taller();
        t.fizzbuzz(100);
    }
    
}
